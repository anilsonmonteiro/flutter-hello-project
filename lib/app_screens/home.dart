import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            alignment: Alignment.topLeft,
            color: Colors.deepPurple,
            margin: EdgeInsets.only(
                left: 10.0, top: 10.0, bottom: 10.0, right: 10.0),
            child: Column(
              children: <Widget>[
                TopImage(),
                Row(
                  children: <Widget>[
                    FlightImageAsset(image: 'image/santiago.jpg'),
                    FlightImageAsset(image: 'image/santiago.jpg'),
                    FlightImageAsset(image: 'image/santiago.jpg'),
                  ],
                ),
              ],
            )));
  }
}

class FlightImageAsset extends StatelessWidget {
  String image;
  FlightImageAsset({Key key, @required this.image});
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage(this.image);
    Image image = Image(image: assetImage, width: 250.0, height: 250.0);
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(top: 60.0, left: 1.0),
      child: image,
    );
  }
}

class TopImage extends StatelessWidget {
  String image;

  TopImage({Key key, @required this.image});
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage(this.image);
    Image image = Image(image: assetImage, width: 900.0, height: 600.0);
    return Container(
      alignment: Alignment.topLeft,
      margin: EdgeInsets.only(top: 0.0, left: 0.0),
      child: image,
    );
  }
}
